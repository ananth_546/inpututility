organization := "com.marriott.elevate"

name := "test-framework"

version := "0.1-SNAPSHOT"

scalaVersion := "2.11.11"

crossPaths := false

libraryDependencies ++= Seq(
  "org.slf4j" % "slf4j-api" % "1.7.2",
  "ch.qos.logback" % "logback-classic" % "1.0.7",
  "org.scalactic" %% "scalactic" % "3.0.4",
  "org.scalatest" %% "scalatest" % "3.0.4" % "test",
  "org.apache.spark" %% "spark-sql" % "2.2.1", //% Provided,
  "org.apache.spark" %% "spark-sql-kafka-0-10" % "2.2.1",// % Provided,
  "org.postgresql" % "postgresql" % "42.1.4",
  "org.json4s" %% "json4s-native" % "3.2.11",
  "joda-time" % "joda-time" % "2.3",
  "com.github.pureconfig" %% "pureconfig" % "0.9.0",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.1.1",
  "com.github.scopt" %% "scopt" % "3.7.0",
  "org.apache.kafka" % "kafka-clients" % "0.10.0.1"
)

assemblyMergeStrategy in assembly := {
  case m if m.toLowerCase.endsWith("manifest.mf") => MergeStrategy.discard
  case m if m.toLowerCase.matches("meta-inf.*\\.sf$") => MergeStrategy.discard
  case "log4j.properties" => MergeStrategy.discard
  case m if m.toLowerCase.startsWith("meta-inf/services/") => MergeStrategy.filterDistinctLines
  case _ => MergeStrategy.first
}
