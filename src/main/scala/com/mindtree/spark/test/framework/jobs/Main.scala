package com.mindtree.spark.test.framework.jobs
import java.io.File
import com.typesafe.scalalogging.LazyLogging

import scala.util.matching.Regex
import scala.xml.Elem


object Main extends ElevateApp with LazyLogging {

  val path = "D:\\TestCase"

  logger.info("update file path::" + sampleUpdate + "sample delete::" + sampleDelete)

  logger.info("autoReg::" + autoRegression(path))

  def autoRegression(path: String) = {

    val insertFile = readFile(sampleInsert)

    val updateFile = readFile(sampleUpdate)

    val deleteFile = readFile(sampleDelete)

    val pattern = "\\<COL name=\"ROW_ID\" (.*?)>(.*?)</COL>".r

    val rowIdInInsertFile = getMatchingString(insertFile, pattern)

    val rowIdInUpdateFile = getMatchingString(updateFile, pattern)

    val rowIdInDeleteFile = getMatchingString(deleteFile, pattern)

    for (i <- 1 to 7) {

      val insertXmlString = insertFile.replace(rowIdInInsertFile.get, "<COL name=\"ROW_ID\" isKey=\"1\" datatype=\"vargraphic\">" + i + "</COL>")

      val updateXmlString = updateFile.replace(rowIdInUpdateFile.get, "<COL name=\"ROW_ID\" isKey=\"1\" datatype=\"vargraphic\">" + i + "</COL>")

      val deleteXmlString = deleteFile.replace(rowIdInDeleteFile.get, "<COL name=\"ROW_ID\" isKey=\"1\" datatype=\"vargraphic\">" + i + "</COL>")

      val dir = new File(path.concat("\\TC" + i))

      val insertXml = toXmlElem(insertXmlString)
      val deleteXml = toXmlElem(deleteXmlString)
      val updateXml = toXmlElem(updateXmlString)

      dir.mkdirs()

      i match {

        case 1 =>
          logger.info("case 1")
          createWindows(dir.toString, List("insert.xml", "insert.xml", "update.xml", "delete.xml"), List(insertXml, insertXml, updateXml, deleteXml))

        case 2 =>
          logger.info("case 2")
          createWindows(dir.toString, List("delete.xml", "insert.xml", "insert.xml", "update.xml", "delete.xml"), List(deleteXml, insertXml, insertXml, updateXml, deleteXml))

        case 3 =>
          logger.info("case 3")
          val xmlList = modNumUpdate(List("update.xml", "insert.xml", "insert.xml", "update.xml", "delete.xml"), List(updateXml, insertXml, insertXml, updateXml, deleteXml))
          createWindows(dir.toString, List("update.xml", "insert.xml", "insert.xml", "update.xml", "delete.xml"), xmlList)

        case 4 =>
          logger.info("case 4")
          val xmlList = modNumUpdate(List("update.xml", "delete.xml", "insert.xml", "insert.xml", "update.xml", "delete.xml"), List(updateXml, deleteXml, insertXml, insertXml, updateXml, deleteXml))
          createWindows(dir.toString, List("update.xml", "delete.xml", "insert.xml", "insert.xml", "update.xml", "delete.xml"), xmlList)

        case 5 =>
          logger.info("case 5")
          val xmlList = modNumUpdate(List("update.xml", "update.xml", "delete.xml"), List(updateXml, updateXml, deleteXml))
          createWindows(dir.toString, List("update.xml", "update.xml", "delete.xml"), xmlList)

        case 6 =>
          logger.info("case 6")
          val xmlList = modNumUpdate(List("delete.xml", "update.xml", "update.xml", "delete.xml"), List(deleteXml, updateXml, updateXml, deleteXml))
          createWindows(dir.toString, List("delete.xml", "update.xml", "update.xml", "delete.xml"), xmlList)

        case 7 =>
          logger.info("case 7")
          createWindows(dir.toString, List("delete.xml", "delete.xml"), List(deleteXml, deleteXml))

      }
    }


    def createWindows(directory: String, fileList: List[String], xmlList: List[Elem]) = {

      val arr = new Array[File](fileList.size * 3)

      for (i <- 0 to fileList.size - 1) {
        arr(i) = new File(directory.concat("\\W" + (i + 1) + "\\Input\\Kafka\\" + kafkaTopic))
        arr(i + 1) = new File(directory.concat("\\W" + (i + 1) + "\\Input\\DB\\"))
        arr(i + 2) = new File(directory.concat("\\W" + (i + 1) + "\\Output\\DB\\" + tableName ))
        arr(i).mkdirs()
        arr(i + 1).mkdirs()
        arr(i + 2).mkdirs()
        scala.xml.XML.save(arr(i).toString + "\\" + fileList(i), xmlList(i))
      }
    }
  }


  private def toXmlElem(xmlString: String) = {
    scala.xml.XML.loadString(xmlString)
  }

  def modNumUpdate(fileList:List[String], xmlList:List[Elem]) ={
    var (count,index) = (0,0)

    for(i <- 0 to xmlList.size-1){
       if(fileList(i) == "update.xml") {
         count = count + 1
         index = i
       }
    }

    val newXmlList =  if(count >= 2){
      val newUpdate = xmlList(index).toString
      val pattern = "\\<COL datatype=\"decimal\" name=\"MODIFICATION_NUM\">(.*?)</COL>".r
      val newUpdateString = getMatchingString(newUpdate,pattern)
      val updateXmlString = newUpdate.replace(newUpdateString.get, "<COL datatype=\"decimal\" name=\"MODIFICATION_NUM\">" + count + "</COL>")
      val sampleUpdate = toXmlElem(updateXmlString)
      xmlList.updated(index,sampleUpdate)
      } else xmlList

    newXmlList
  }

  private def readFile(filePath:String) = {
    scala.io.Source.fromFile(filePath).mkString
  }

  private def getMatchingString(file: String, pattern: Regex) = {
    pattern.findFirstIn(file)
  }
}

