package com.mindtree.spark.test.framework.jobs

import scala.util.{Failure, Success, Try}

/**
  * Case Class used by Scopt Option Parser
  *
  * @param connectionconfig Path defining the connection configuration file
  * @param jobconfig        Path defining the job configuration file
  */
case class CommandLineArgs(sampleinsert : String =null, sampledelete : String = null,sampleupdate :String = null,kafkatopic:String = null,tablename:String=null)

/**
  * Class using Scopt for argument parsing.
  */
class ElevateApp extends App {

  val parser = new scopt.OptionParser[CommandLineArgs]("scopt") {
    head("scopt", "3.x")
    opt[String]('d', "sampleDelete") required() action { (x, c) =>
      c.copy(sampledelete = x) } text("sample delete file")

    opt[String]('u', "sampleUpdate") required() action { (x, c) =>
      c.copy(sampleupdate = x) } text("sample update file")

    opt[String]('i', "sampleInsert") required() action { (x, c) =>
      c.copy(sampleinsert = x) } text("sample insert file")

    opt[String]('k', "kafkaTopic") required() action { (x, c) =>
      c.copy(kafkatopic = x) } text("sample db file")

    opt[String]('t', "tableName") required() action { (x, c) =>
      c.copy(tablename = x) } text("table name")

  }

  var sampleUpdate :String = _

  var sampleDelete : String =_

  var sampleInsert : String =_

  var kafkaTopic : String =_

  var tableName :String =_

  parser.parse(args, CommandLineArgs()) match {

    case Some(config) =>

      sampleUpdate = config.sampleupdate
      sampleDelete = config.sampledelete
      sampleInsert = config.sampleinsert
      kafkaTopic = config.kafkatopic
      tableName = config.tablename

  }

}